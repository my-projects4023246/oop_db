<?php


    class db_database
    { 
 
        // Member Variables
        private $conn;
        private $id;
        public $name ;
        public $age;
        //constructor

        function __construct($conn){
            // echo "constructor";
            $this->conn = $conn;
            // $this->connect($this->dbname, $this->user,$this->server,$this->password);
            // $this->create();
            // $this->insert();
            // $this->update();
            // $this->delete();
            // $this->select();
        }


        //Member Methods / Functions
        // public function connect($dbname, $user, $server, $password)
        // {
        //     // echo "function";
        //     // Create connection
        //     $this->conn = new mysqli($server, $user, $password, $dbname);
        //     // Check connection
        //     if ($this->conn->connect_error) {
        //     die("Connection failed: " . $this->conn->connect_error);
        //     }
        //     else {
        //         echo "Connection Successful <br><br>";
        //     }

        // }

        public function create()
        {
            $sql = "CREATE TABLE IF NOT EXISTS pick (
                id INT(11) AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(20) NOT NULL,
                age INT(10) NOT NULL)";

            if ($this->conn->query($sql) === TRUE) {
                echo "Table Created Successfully <br><br>";
            }
            else {
                echo "Error: " .$sql .$this->conn->error;
            }
        }

        public function insert()
        {
            $sql = "INSERT INTO  pick (name, age) VALUES ('$this->name', $this->age)";

            if ($this->conn->query($sql) === TRUE) {
                echo "Information Inserted Successfully <br><br>";
            }
            else {
                echo "Error: " .$sql .$this->conn->error;
            }
        }

        public function update()
        {
            $sql = "UPDATE pick SET age=23 WHERE id=1";

            if ($this->conn->query($sql) === TRUE) {
                echo "Table Updated Successfully <br><br>";
            }
            else {
                echo "Error: " .$sql .$this->conn->error;
            }
        }


        public function delete()
        {
            $sql =  "DELETE FROM pick WHERE age=21";

            if ($this->conn->query($sql) === TRUE) {
                echo "Record Deleted Successfully <br><br>";
            }
            else {
                echo "Error: " .$sql .$this->conn->error;
            }
        }

        public function select()
        {
            $sql = "SELECT * FROM pick";
            $result = $this->conn->query($sql);

            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    echo "<br> id: " .$row['id']. " -- Name: " .$row['name']. " -- Age: " .$row['age']. "<br>"; 
                }
            }
            else {
                echo "0 results";
            }
        }

       

    }

    

?>